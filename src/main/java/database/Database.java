/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;
//Singleton pattern

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Windows
 */
public class Database {

    private static Database insance = new Database();
    private Connection conn;

    private Database() {
    }

    public static Database getInstance() {
        String dbPath = "./db/CoffeeStore.db";
        try {
            if (insance.conn == null || insance.conn.isClosed()) {
                Class.forName("org.sqlite.JDBC");
                insance.conn = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
                System.out.println("Database connection");
            }
        } catch (ClassNotFoundException ex) {
            System.out.println("Error: JDBC is not exist");

        } catch (SQLException ex) {
            System.out.println("Error: Database cannot connect");

        }
        return insance;

    }

    public static void close() {
        try {
            if (insance.conn != null || !insance.conn.isClosed()) {
                insance.conn.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }

        insance.conn = null;
    }

    public Connection getConnection() {
        return insance.conn;
    }
}
